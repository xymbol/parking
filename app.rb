require "forwardable"
require "tilt"
require "tilt/haml"
require "rack/utils"
require_relative "parking"

Haml::Options.defaults[:attr_wrapper] = %(")

# Provides an app for parking allocation.
class App
  extend Forwardable

  attr_reader :parking

  def initialize(drivers, spots)
    @parking = Parking.new drivers, spots
  end

  def call(env)
    [200, { "Content-type" => "text/html" }, [render]]
  end

  def render
    template = Tilt.new "templates/index.haml"
    template.render self
  end

  def_delegators :parking, :drivers, :spots, :start, :time, :allocation,
    :elapsed_weeks, :turn

  def escape_html(string)
    Rack::Utils.escape_html string
  end
  alias_method :h, :escape_html

  def count_from(index)
    "%d" % (index + 1)
  end
end
