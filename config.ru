require "rack"
require "rack/contrib/try_static"
require "dotenv"
require_relative "app"

Dotenv.load

drivers = ENV["PARKING_DRIVERS"].split(":").freeze
spots   = ENV["PARKING_SPOTS"].to_i.freeze

use Rack::ConditionalGet
use Rack::ETag
use Rack::TryStatic, urls: [""], root: "public"

run App.new drivers, spots
