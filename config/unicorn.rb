worker_processes Integer(ENV["WEB_CONCURRENCY"] || 3)
timeout 15
preload_app true

before_fork do |server, worker|
  Signal.trap "TERM" do
    Process.kill "QUIT", Process.pid
  end
end

after_fork do |server, worker|
  Signal.trap "TERM", "IGNORE"
end
