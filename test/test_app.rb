require_relative "helper"
require_relative "../app"

class AppTest < Minitest::Test
  include Rack::Test::Methods

  attr_reader :app

  def setup
    @app = App.new %w(Moe Larry Curly), 2
  end

  def test_get
    get "/"
    assert last_response.ok?
    assert last_response.body.include?("Parking Roulette")
  end
end
