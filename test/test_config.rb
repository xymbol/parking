require "singleton"
require_relative "helper"

class TestConfig < Minitest::Test
  class Stack
    include Singleton

    attr_reader :app

    def initialize
      @app = Rack::Builder.parse_file(path).first
    end

    def path
      File.expand_path("../../config.ru", __FILE__)
    end
  end

  include Rack::Test::Methods

  attr_reader :app

  def setup
    @app = Stack.instance.app
  end

  def test_root
    get "/"
    assert last_response.ok?
    assert last_response.body.include?("Parking Roulette")
  end

  def test_parking_icon
    get "/images/parking-icon.png"
    assert last_response.ok?
    assert_equal "image/png", last_response.content_type
  end

  def test_favicon
    get "/favicon.ico"
    assert last_response.ok?
    assert_equal 4286, last_response.content_length
  end

  def test_robots
    get "/robots.txt"
    assert last_response.ok?
    assert_match /^User-agent:/, last_response.body
  end
end
