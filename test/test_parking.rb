require_relative "helper"

class TestParking < Minitest::Test
  attr_reader :parking, :drivers, :spots, :start, :time

  def setup
    @drivers = %w(Sabrina Jill Kelly)
    @spots   = 2
    @start   = Time.new 1976, 9, 22
    @time    = Time.new 1976, 9, 29
    @parking = Parking.new drivers, spots, start, time
  end

  def test_allocations
    assert_equal 3, parking.allocations.size
  end

  def test_allocation
    assert_equal parking.allocations[1], parking.allocation
  end

  def test_turn
    assert_equal 1, parking.turn
  end

  def test_elapsed_weeks
    assert_equal 1, parking.elapsed_weeks
  end

  %w(drivers spots start time).each do |name|
    define_method "test_#{name}" do
      assert_equal send(name), parking.send(name)
    end
  end

  def test_allocations_without_combination
    other = Parking.new %w(Starsky Hutch), 2
    assert_equal 1, other.allocations.size
  end
end
